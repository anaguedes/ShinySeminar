source("global.R")

server <- function(input, output) {
  
datatoplot = reactive({
  datatoplot = subset(data, Year >= input$daterange[1] & Year <= input$daterange[2])
})
  
output$topalbums = renderPlot({
  data = datatoplot()
  
  variable = input$checkGroup
  if (variable == 1){
    variable_name = "Artist"
  } else if (variable == 2){
    variable_name = "Genre"
  } else {
    variable_name = "Year"
  }
  
  data = ddply(data, c(variable_name), summarise, n = length(Number))
  topdata = data[order(-data$n),]
  topdata = topdata[1:10,]
  n = "n"
  
  ggplot(topdata) + 
    geom_bar(aes_string(variable_name, n), stat = "identity") +
    xlab(variable_name) + ylab("Number of Albums") + ggtitle(paste0("Top 10 ", variable_name))
  
})

output$data = renderDataTable({
  data = datatoplot()
  data = data[order(data$Year), ]
})

rankingartist = reactive({
  ranking = input$ranking
  topalbums = data[1:ranking,]
  
  rankingartist = subset(data, Artist %in% topalbums$Artist)
})


output$log1 = renderText({
 paste0("Looking at the timeline for the Artists in the top ", input$ranking, " albums.") 
})

output$artisttimeline = renderPlot({
  ggplot(rankingartist()) + 
    geom_line(aes(Year, Number, colour = Artist)) +
    geom_point(aes(Year, Number, colour = Artist)) +
    xlab("Year") + ylab("Album ranking")
})

} # server
