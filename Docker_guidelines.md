# Docker commands for deployment of shiny seminar container  

~/ShinySeminar  
docker build -t shinyseminarimage .  
docker image tag shinyseminarimage rmlbbpapp5012.rmgv.royalmailgroup.net:80/shinyseminarimage  
docker push rmlbbpapp5012.rmgv.royalmailgroup.net:80/shinyseminarimage  


ssh anag@10.107.72.103  
sudo docker pull localhost:5003/shinyseminarimage  
sudo docker run --name ShinySeminar -p 1811:8080 localhost:5003/shinyseminarimage  


# port mapping on the server  
cd /etc/httpd/conf.d/  
sudo vim DataScience-host.conf  

<Location /ShinySeminar/>  
  ProxyPass http://localhost:1811/ connectiontimeout=5 timeout=600  
  ProxyPassReverse http://localhost:1811/  
</Location>  

<Location "/ShinySeminar/websocket/">  
    ProxyPass ws://localhost:1811/ShinySeminar/websocket/  
    ProxyPassReverse ws://localhost:1811/ShinySeminar/websocket/  
</Location>  

# RESTART Apache server
sudo systemctl restart httpd  
(after changing apache config file)  
sudo systemctl status httpd  